import React, { Component } from 'react';
import Header from './components/Header';
import Form from "./components/Form";
import Events from './components/Events'

class App extends Component {

  token = '2T3OS64BFXKVHUBP5YQO';
  sort = 'date';

  state = {
    categories : [],
    events: []
  };

  componentDidMount(){
    this.getCategories()
  }

  getEvents = async(searched) => {

      let url = `https://www.eventbriteapi.com/v3/events/search/?q=${searched.name}&sort_by=${this.sort}&categories=${searched.category}&token=${this.token}&locale=es_ES`;
      await fetch(url).then(response => {
          return response.json()
      }).then(events => {
        console.log(events)
          this.setState({
              events: events.events
          })
      }); //it avoid stop program
  };

  getCategories = async() =>{

    let url = `https://www.eventbriteapi.com/v3/categories/?token=${this.token}&locale=es_ES`;

    await fetch(url).then(response => {
      return response.json()
    }).then(categories => {
      this.setState({
          categories: categories.categories
      })
    }); //it avoid stop program
  };

  render() {
    return (
      <div className="App">
        <Header/>
        <div className={"uk-container"}>
          <Form
              categories = {this.state.categories}
              getEvents = {this.getEvents}
          />

          <Events
            events={this.state.events}
          />

        </div>
      </div>
    );
  }
}

export default App;
