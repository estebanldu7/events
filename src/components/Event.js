import React from 'react';

const Event = props => {

    const {name} = props.info;
    let desc = props.info.description.text;

    if(!name){
        return null;
    }

    if(desc.length > 250){
        desc = desc.substr(0,250)
    }

    return(
        <div>
            <div className={"uk-card uk-card-default"}>
                <div className={"uk-card-media-top"}>
                    {props.info.logo !== null ?
                        <img src={props.info.logo.url} alt={props.info.logo.text}/>
                     : ''
                    }

                </div>
                <div className={"uk-card-body"}>
                    <h3 className={"uk-card-title"}>{props.info.name.text}</h3>
                    <p>{desc}</p>
                </div>
                <div className={"uk-card-footer"}>
                    <a className={"uk-button uk-button-secondary" } href={props.info.url} target={"_blank"}>Mas Informacion</a>
                </div>
            </div>
        </div>
    )
};

export default Event