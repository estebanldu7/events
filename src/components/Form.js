import React from 'react';

class Form extends React.Component{

    nameEventRef = React.createRef();
    categoryRef = React.createRef();

    searchEvent = (e) => {
        e.preventDefault();

        const dataSearch = {
            name: this.nameEventRef.current.value,
            category: this.categoryRef.current.value
        };

        this.props.getEvents(dataSearch)
    };

    showOptions = (key) => {
        const categry = this.props.categories[key];
        const {id, name_localized} = categry;

        if(!id || !name_localized){
            return null;
        }

        //fill the select
        return(<option key={id} value={id}>{name_localized}</option>)
    };

    render(){
        const categories = Object.keys(this.props.categories);

        return(
            <form onSubmit={this.searchEvent}>
                <fieldset className={"uk-fieldset uk-margin"}>
                    <legend className={"uk-legend uk-text-center"}>
                        Busca tu evento por nombre o categoria
                    </legend>
                    <div className={"uk-column-1-3@m uk-margin"}>
                        <div uk-margin="true">
                            <input className={"uk-input"} type={"text"} ref={this.nameEventRef} placeholder={"Nombre de evento o ciudad"}/>
                        </div>
                        <div uk-margin="true">
                            <select className={"uk-select"} ref={this.categoryRef}>
                                {categories.map(this.showOptions)}
                            </select>
                        </div>
                        <div uk-margin="true">
                            <button className={"uk-button uk-button-danger"}>Buscar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        )
    }
}

export default Form